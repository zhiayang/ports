#!/bin/bash


if [ "$1" = "" -o "$2" = "" ]
then
	echo "Please provide the folders in the form ./genpatch [ORIGINAL] [MODIFIED]"
fi

diff -x ".git" -x ".svn" -purN $1 $2 > patches/$2.patch

